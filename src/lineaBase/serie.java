package lineaBase;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Programa: Generar la sumatoria de la siguiente serie : 11^0 + 1^13 - 17^1 -
 * 2^19 + 23^3 + 5^27 - 29^8 - 13^31 + 37^21 +34^41
 *
 * @author David Robalino
 */

public class serie {
	static PrintStream print;

	// M�todo principal
	public static void main(String args[]) {
			// Creacion del archivo .txt donde se guardara el resultado
			try (Formatter salidaArchivo = new Formatter("DavidRobalino.txt")) {
			// Creacion de objeto input
			@SuppressWarnings("resource")
			Scanner entrada = new Scanner(System.in);
			// Variables
			int X = 11;
			int Y = 1;
			int a = 0;
			int b = 13;
			int number;
			double suma = 0;
			salidaArchivo.format("Ingrese un n�mero para generar la serie: \n");
			System.out.println("Ingrese un n�mero para generar la serie: \n");
			number = entrada.nextInt();
			salidaArchivo.format(String.valueOf(number));
			salidaArchivo.format("Serie generada: \n");
			System.out.print("Serie generada: \n");
			for (int i = 1; i <= number; i++) {
				// Genera los signos de la serie
				int signo = 1;
				if (i % 2 == 0) {
					signo = -1;
				}
				// Imprime la serie en consola
				salidaArchivo.format((X * signo) + "e" + a + ", " + (Y * signo) + "e" + b + ", ");
				System.out.print((X * signo) + "e" + a + ", " + (Y * signo) + "e" + b + ", ");
				// Realiza la sumatoria de la serie
				suma = suma + (Math.pow((X * signo), a)) + (Math.pow((Y * signo), b));
				// Genera los valores X,Y(bases) y a,b(exponentes) de la serie
				a = a + Y;
				Y = a + Y;
				b++;
				boolean aux = false;
				int numero = b;
				do {
					if (esPrimo(numero)) {
						aux = true;
						X = numero;
					}
					numero++;
				} while (!aux);
				int b1 = X + 1;
				aux = false;
				numero = b1;
				do {
					if (esPrimo(numero)) {
						aux = true;
						if (i == 2) {
							b = numero - 2;
						} else {
							b = numero;
						}

					}
					numero++;
				} while (!aux);
			}
			salidaArchivo.format("\nLA SUMA DE LA SERIE ES: " + suma);
			System.out.print("\nLA SUMA DE LA SERIE ES: " + suma);
			// --Fin de generar la serie
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// M�todo que determina si un numero es primo o no
	private static boolean esPrimo(int numero) {
		// Variables del m�todo esPrimo
		int contador = 2;
		boolean primo = true;
		while ((primo) && (contador != numero)) {
			if (numero % contador == 0) {
				primo = false;
			}
			contador++;
		}
		return primo;
	}

}
