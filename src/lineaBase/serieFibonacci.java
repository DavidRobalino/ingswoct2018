package lineaBase;

import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 * Programa: Generar la serie de fibonacci dado una numero
 *
 * @author David Robalino
 */
public class serieFibonacci {

	public static void main(String[] args) {
		try (Formatter salidaArchivo = new Formatter("DavidRobalinoFibonacci.txt")) {
			int exponente = 1;
			int fibonaci1 = 1;
			int fibonaci2 = 1;
			int primo = 1;
			int signo_Fibonaci = 1;
			int signo_primo = 1;
			int contador_signo = 1;
			salidaArchivo.format("*********** Serie de fibonacci  ******************\n");
			System.out.println("*********** Serie de fibonacci  ******************\n");
			// el numero para generar la serie ser� 8
			for (int i = 0; i < 8; i++) {
				int numeradorFibonaci = fibonaci1 + fibonaci2;
				boolean esPrimo = false;
				do {
					primo++;
					esPrimo = esPrimo(primo);
				} while (!esPrimo);
				String SIGNO = "";
				if (exponente % 2 == 0) {
					signo_Fibonaci = -1;
					if (signo_Fibonaci * signo_primo == 1) {
						SIGNO = "+";
					} else {
						SIGNO = "-";
					}
					salidaArchivo.format(SIGNO + "(" + primo + "/" + numeradorFibonaci + ")^" + exponente);
					System.out.println(SIGNO + "(" + primo + "/" + numeradorFibonaci + ")^" + exponente);
				} else {
					signo_Fibonaci = 1;
					if (signo_Fibonaci * signo_primo == 1) {
						SIGNO = "+";
					} else {
						SIGNO = "-";
					}
					salidaArchivo.format(SIGNO + "(" + numeradorFibonaci + "/" + primo + ")^" + exponente);
					System.out.println(SIGNO + "(" + numeradorFibonaci + "/" + primo + ")^" + exponente);
				}
				if (contador_signo == 2) {
					signo_primo = signo_primo * (-1);
					contador_signo = 0;
				}
				contador_signo++;
				exponente++;
				fibonaci1 = fibonaci2;
				fibonaci2 = numeradorFibonaci;
				numeradorFibonaci = fibonaci1 + fibonaci2;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// m�todo que determina si un numero es primo
	public static boolean esPrimo(int numero) {
		int contador = 2;
		boolean primo = true;
		while ((primo) && (contador != numero)) {
			if (numero % contador == 0)
				primo = false;
			contador++;
		}
		return primo;
	}

}
